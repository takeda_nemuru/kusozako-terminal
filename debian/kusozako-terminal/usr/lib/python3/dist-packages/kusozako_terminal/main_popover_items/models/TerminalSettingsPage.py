
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

TERMINAL_SETTINGS_PAGE = {
    "page-name": "terminal-settings",
    "items": [
        {
            "type": "back-switcher",
            "title": _("Back"),
            "message": "delta > switch stack to",
            "user-data": "main",
        },
        {
            "type": "separator"
        },
        {
            "type": "switcher",
            "title": _("Cursor Shape"),
            "message": "delta > switch stack to",
            "user-data": "cursor-shape",
        },
        {
            "type": "separator"
        },
        {
            "type": "check-action-boolean",
            "title": _("Copy on Select"),
            "message": "delta > settings",
            "user-data": ("vte", "copy_on_select", False),
            "user-data-false": ("vte", "copy_on_select", True),
            "close-on-clicked": False,
            "registration": "delta > register settings object",
            "query": "delta > settings",
            "query-data": ("vte", "copy_on_select", False),
            "check-value": True
        },
        {
            "type": "separator"
        },
        {
            "type": "label",
            "title": _("Color :")
        },
        {
            "type": "simple-action",
            "title": _("Text Color"),
            "message": "delta > action",
            "user-data": ("vte.settings.text-color", None),
            "close-on-clicked": True
        },
        {
            "type": "simple-action",
            "title": _("Background Color"),
            "message": "delta > action",
            "user-data": ("vte.settings.background-color", None),
            "close-on-clicked": True
        },
        {
            "type": "separator"
        },
        {
            "type": "label",
            "title": _("Font :")
        },
        {
            "type": "check-action-boolean",
            "title": _("Allow Bold"),
            "message": "delta > settings",
            "user-data": ("vte", "allow_bold", False),
            "user-data-false": ("vte", "allow_bold", True),
            "close-on-clicked": False,
            "registration": "delta > register settings object",
            "query": "delta > settings",
            "query-data": ("vte", "allow_bold", False),
            "check-value": True
        },
        {
            "type": "simple-action",
            "title": _("Select Font"),
            "message": "delta > action",
            "user-data": ("vte.settings.font", None),
            "close-on-clicked": True
        }
    ]
}
