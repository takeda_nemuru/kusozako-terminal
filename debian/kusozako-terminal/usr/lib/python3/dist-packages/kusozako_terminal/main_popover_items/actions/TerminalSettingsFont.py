
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Pango
from libkusozako3.font_chooser.FontChooser import DeltaFontChooser
from .TerminalSettings import AlfaTerminalSettings


class DeltaTerminalSettingsFont(AlfaTerminalSettings):

    ACTION_ID = "vte.settings.font"

    def _action(self, param=None):
        # default value set by vte itself.
        # so do not need default value.
        query = "vte", "font", ""
        current_font = self._enquiry("delta > settings", query)
        font_description = Pango.font_description_from_string(current_font)
        selected_font = DeltaFontChooser.get_font(
            self,
            font_description
            )
        if selected_font is None:
            return
        user_data = "vte", "font", selected_font.to_string()
        self._raise("delta > settings", user_data)
