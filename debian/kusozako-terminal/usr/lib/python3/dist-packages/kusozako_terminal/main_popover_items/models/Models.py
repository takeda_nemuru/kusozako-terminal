
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from .SwitcherItem import SWITCHER_ITEM
from .TerminalSettingsPage import TERMINAL_SETTINGS_PAGE
from .CursorShapePage import CURSOR_SHAPE_PAGE


class DeltaModels(DeltaEntity):

    def __init__(self, parent):
        self._parent = parent
        self._raise(
            "delta > application popover add item",
            ("main", SWITCHER_ITEM)
            )
        self._raise(
            "delta > application popover add page",
            TERMINAL_SETTINGS_PAGE
            )
        self._raise(
            "delta > application popover add page",
            CURSOR_SHAPE_PAGE
            )
