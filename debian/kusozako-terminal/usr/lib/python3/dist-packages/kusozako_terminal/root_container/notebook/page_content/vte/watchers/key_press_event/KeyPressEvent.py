
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from kusozako_terminal import SplitDirection

BINDS = {
    "Shift+Ctrl+C": "delta > copy clipboard",
    "Shift+Ctrl+V": "delta > paste clipboard",
    "Ctrl+D": "delta > close tab",
    "Shift+Ctrl+T": "delta > add new tab",
    "Shift+Ctrl+W": "delta > close tab",
    "Shift+Ctrl+H": "delta > split",
    "Shift+Ctrl+J": "delta > split",
    "Shift+Ctrl+K": "delta > split",
    "Shift+Ctrl+L": "delta > split",
    "Ctrl+Left": "delta > tab move to left",
    "Ctrl+Right": "delta > tab move to right"
    }

PARAM = {
    "Shift+Ctrl+H": (Gtk.Orientation.HORIZONTAL, SplitDirection.START),
    "Shift+Ctrl+J": (Gtk.Orientation.VERTICAL, SplitDirection.END),
    "Shift+Ctrl+K": (Gtk.Orientation.VERTICAL, SplitDirection.START),
    "Shift+Ctrl+L": (Gtk.Orientation.HORIZONTAL, SplitDirection.END)
    }


class DeltaKeyPressEvent(DeltaEntity):

    def _on_key_press(self, vte, event):
        accel_label = Gtk.accelerator_get_label(event.keyval, event.state)
        message = BINDS.get(accel_label, None)
        if message is not None:
            param = PARAM.get(accel_label, None)
            self._raise(message, param)
        return message is not None

    def __init__(self, parent):
        self._parent = parent
        vte = self._enquiry("delta > vte")
        vte.connect("key-press-event", self._on_key_press)
