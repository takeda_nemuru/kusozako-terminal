
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .Setting import AlfaSetting


class DeltaAllowBold(AlfaSetting):

    KEY = "allow_bold"
    DEFAULT = False

    def _reset_setting(self, vte, setting):
        vte.set_allow_bold(setting)
