
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio


class FoxtrotProcessName:

    def get_name_for_pid(self, pid):
        if pid != self._pid:
            path = "/proc/{0}/cmdline".format(pid)
            self._process_name = Gio.File.new_for_path(path)
        self._pid = pid
        success, contents_bytes, _ = self._process_name.load_contents()
        # replace null byte with space
        return contents_bytes.decode("utf8").replace("\0", " ")

    def __init__(self):
        self._pid = ""
