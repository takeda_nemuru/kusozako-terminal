
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from .terminator.Terminator import DeltaTerminator
from .notebook.Notebook import DeltaNotebook
from .Paned import DeltaPaned


class DeltaRootContainer(Gtk.Bin, DeltaEntity):

    def _delta_call_split(self, user_data):
        notebook, orientation, direction = user_data
        parent_container = notebook.get_parent()
        parent_container.remove(notebook)
        paned = DeltaPaned.new(self, notebook, orientation, direction)
        parent_container.add(paned)
        self.show_all()

    def _delta_call_delete_notebook(self, notebook):
        parent_container = notebook.get_parent()
        parent_container.remove(notebook)
        if parent_container != self:
            parent_container.reparent_unremoved_child()
        notebook.destroy()
        if not self.get_children():
            self.add(DeltaNotebook(self))

    def __init__(self, parent):
        self._parent = parent
        Gtk.Bin.__init__(self)
        self.add(DeltaNotebook(self))
        self._raise("delta > add to container", self)
        terminator = DeltaTerminator(self)
        self._raise("delta > register terminator object", terminator)
