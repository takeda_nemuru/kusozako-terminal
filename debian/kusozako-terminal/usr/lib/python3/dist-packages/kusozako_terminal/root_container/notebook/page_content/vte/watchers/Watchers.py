
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from .button_press_event.ButtonPressEvent import DeltaButtonPressEvent
from .key_press_event.KeyPressEvent import DeltaKeyPressEvent
from .SelectionChanged import DeltaSelectionChanged


class DeltaWatchers(DeltaEntity):

    def _delta_call_copy_clipboard(self):
        vte = self._enquiry("delta > vte")
        vte.copy_clipboard_format(1)

    def _delta_call_paste_clipboard(self):
        vte = self._enquiry("delta > vte")
        vte.paste_clipboard()

    def __init__(self, parent):
        self._parent = parent
        DeltaButtonPressEvent(self)
        DeltaKeyPressEvent(self)
        DeltaSelectionChanged(self)
