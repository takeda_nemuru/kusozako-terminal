
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from .watchers.Watchers import EchoWatchers
from .page_content.PageContent import DeltaPageContent


class DeltaNotebook(Gtk.Notebook, DeltaEntity):

    def _delta_call_append_page(self, user_data):
        page_content, page_label = user_data
        current_page = self.get_current_page()+1
        page_index = self.insert_page(page_content, page_label, current_page)
        self.set_tab_reorderable(page_content, True)
        self.show_all()
        self.set_current_page(page_index)

    def _delta_call_remove_page(self, page_content):
        self.detach_tab(page_content)
        page_content.destroy()
        if not self.get_children():
            self._raise("delta > delete notebook", self)

    def _delta_call_split(self, user_data):
        self._raise("delta > split", (self, *user_data))

    def _delta_call_add_new_tab(self, directory=None):
        DeltaPageContent.new_for_directory(self, directory)

    def _delta_call_tab_move_to_left(self):
        self.prev_page()

    def _delta_call_tab_move_to_right(self):
        self.next_page()

    def _delta_info_notebook(self):
        return self

    def __init__(self, parent):
        self._parent = parent
        Gtk.Notebook.__init__(self, scrollable=False)
        EchoWatchers(self)
        DeltaPageContent.new_for_directory(self)
