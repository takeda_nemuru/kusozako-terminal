
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import gettext
import locale
import gi

gi.require_version('Vte', '2.91')

GROUP_ID = "com.gitlab.kusozako-tech"
VERSION = "2022.11.11"
APPLICATION_NAME = "kusozako-terminal"
APPLICATION_ID = "{}.{}".format(GROUP_ID, APPLICATION_NAME)

locale.setlocale(locale.LC_ALL, None)
gettext.install(
    APPLICATION_NAME,
    "/usr/share/locale",
    names=('gettext', 'ngettext')
    )

LONG_DESCRIPTION = _("""Virtual Termnal Emulator for kusozako project.
This software is licencced under GPL version 3 or any later version.""")

APPLICATION_DATA = {
    "name": APPLICATION_NAME,
    "id": APPLICATION_ID,
    "icon-name": APPLICATION_ID,
    "version": VERSION,
    "short description": "Virtual Terminal Emulator",
    "long-description": LONG_DESCRIPTION,
    "unique-application": False,
    }
