
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later


CURSOR_SHAPE_PAGE = {
    "page-name": "cursor-shape",
    "items": [
        {
            "type": "back-switcher",
            "title": _("Back"),
            "message": "delta > switch stack to",
            "user-data": "terminal-settings",
        },
        {
            "type": "separator"
        },
        {
            "type": "check-action",
            "title": _("Block"),
            "message": "delta > settings",
            "user-data": ("vte", "cursor_shape", 0),
            "close-on-clicked": False,
            "registration": "delta > register settings object",
            "query": "delta > settings",
            "query-data": ("vte", "cursor_shape", 0),
            "check-value": 0
        },
        {
            "type": "check-action",
            "title": _("IBeam"),
            "message": "delta > settings",
            "user-data": ("vte", "cursor_shape", 1),
            "close-on-clicked": False,
            "registration": "delta > register settings object",
            "query": "delta > settings",
            "query-data": ("vte", "cursor_shape", 0),
            "check-value": 1
        },
        {
            "type": "check-action",
            "title": _("Underline"),
            "message": "delta > settings",
            "user-data": ("vte", "cursor_shape", 2),
            "close-on-clicked": False,
            "registration": "delta > register settings object",
            "query": "delta > settings",
            "query-data": ("vte", "cursor_shape", 0),
            "check-value": 2
        },
    ]
}
