
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .TextColor import DeltaTextColor
from .BackgroundColor import DeltaBackgroundColor
from .Font import DeltaFont
from .AllowBold import DeltaAllowBold
from .CursorShape import DeltaCursorShape


class EchoSettings:

    def __init__(self, parent):
        DeltaTextColor(parent)
        DeltaBackgroundColor(parent)
        DeltaFont(parent)
        DeltaAllowBold(parent)
        DeltaCursorShape(parent)
