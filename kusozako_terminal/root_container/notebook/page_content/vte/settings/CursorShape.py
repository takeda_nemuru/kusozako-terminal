
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .Setting import AlfaSetting


class DeltaCursorShape(AlfaSetting):

    KEY = "cursor_shape"
    DEFAULT = False

    def _reset_setting(self, vte, setting):
        vte = self._enquiry("delta > vte")
        vte.set_cursor_shape(setting)
