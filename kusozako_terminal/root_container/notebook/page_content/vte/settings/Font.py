
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Pango
from libkusozako3.Entity import DeltaEntity


class DeltaFont(DeltaEntity):

    def _reset(self, font):
        font_description = Pango.font_description_from_string(font)
        vte = self._enquiry("delta > vte")
        vte.set_font(font_description)

    def receive_transmission(self, user_data):
        group, key, value = user_data
        if group == "vte" and key == "font":
            self._reset(value)

    def __init__(self, parent):
        self._parent = parent
        query = "vte", "font", "Ubuntu Mono 15"
        current_font = self._enquiry("delta > settings", query)
        self._reset(current_font)
        self._raise("delta > register settings object", self)
