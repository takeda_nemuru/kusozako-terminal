
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from libkusozako3.Entity import DeltaEntity
from libkusozako3.message_dialog.MessageDialog import DeltaMessageDialog
from kusozako_terminal import TerminalSignals
from .WorkingDirectory import FoxtrotWorkingDirectory
from .ChildProcess import DeltaChildProcess

DIALOG_MODEL = {
    "default-response": 0,
    "icon-name": "dialog-warning-symbolic",
    "message": "{} is still running.\nare you sure you want to close tab ?",
    "buttons": (_("Cancel"), _("Close Tab"))
    }


class DeltaProcessWatcher(DeltaEntity):

    @classmethod
    def new_for_pid(cls, parent, pid):
        process_watcher = cls(parent)
        process_watcher.set_pid(pid)
        return process_watcher

    def _timeout(self, pid):
        working_directory = self._working_directory.update()
        if working_directory is None:
            return GLib.SOURCE_REMOVE
        signal_param = pid, working_directory, self._child_process.get_name()
        param = TerminalSignals.UPDATE, signal_param
        self._raise("delta > terminal signal", param)
        # self._raise("delta > process updated", user_data)
        return GLib.SOURCE_CONTINUE

    def get_process_name(self):
        return self._child_process.get_name()

    def get_working_directory(self):
        return self._working_directory.get_working_directory()

    def can_close(self):
        process_name = self._child_process.get_name()
        if process_name is None:
            return True
        message = DIALOG_MODEL["message"]
        DIALOG_MODEL["message"] = message.format(process_name)
        response = DeltaMessageDialog.run_for_model(self, DIALOG_MODEL)
        if response != 0:
            self._child_process.kill()
        return response != 0

    def set_pid(self, pid):
        self._working_directory = FoxtrotWorkingDirectory(pid)
        self._child_process = DeltaChildProcess.new_for_pid(self, pid)
        GLib.timeout_add_seconds(1, self._timeout, pid)

    def __init__(self, parent):
        self._parent = parent
