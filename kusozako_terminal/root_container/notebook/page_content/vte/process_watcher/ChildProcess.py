
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from libkusozako3.Entity import DeltaEntity
from .ProcessName import FoxtrotProcessName


class DeltaChildProcess(DeltaEntity):

    @classmethod
    def new_for_pid(cls, parent, pid):
        child_process = cls(parent)
        child_process.set_pid(pid)
        return child_process

    def _get_child_pid(self):
        success, contents_bytes, _ = self._gio_file.load_contents()
        return contents_bytes.decode("utf8").rstrip()

    def kill(self):
        child_pid = self._get_child_pid()
        if child_pid:
            subprocess = Gio.Subprocess.new(["kill", child_pid], 0)
            subprocess.wait(None)

    def get_name(self):
        child_pid = self._get_child_pid()
        if child_pid:
            return self._process_name.get_name_for_pid(child_pid)
        return None

    def set_pid(self, pid):
        # "/proc/{0}/task/{0}/children" is regular file.
        self._process_name = FoxtrotProcessName()
        path = "/proc/{0}/task/{0}/children".format(pid)
        self._gio_file = Gio.File.new_for_path(path)

    def __init__(self, parent):
        self._parent = parent
