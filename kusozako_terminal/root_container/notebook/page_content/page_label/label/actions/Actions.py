
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .Tooltip import DeltaTooltip
from .DisplayText import DeltaDisplayText


class EchoActions:

    def __init__(self, parent):
        DeltaTooltip(parent)
        DeltaDisplayText(parent)
