
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity


class DeltaTabVisibility(DeltaEntity):

    def _on_changed(self, notebook, child_widget, page_number):
        number_of_pages = notebook.get_n_pages()
        notebook.set_show_tabs(number_of_pages > 1)

    def __init__(self, parent):
        self._parent = parent
        notebook = self._enquiry("delta > notebook")
        notebook.connect("page-added", self._on_changed)
        notebook.connect("page-removed", self._on_changed)
