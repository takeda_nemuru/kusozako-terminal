
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from .ProcessParser import FoxtrotProcessParser
from libkusozako3.message_dialog.MessageDialog import DeltaMessageDialog

MESSAGE_TEXT = "<b>Following processes are still running.</b>\n"\
    "\n"\
    "If you select <u>Close Application</u>\n"\
    "All running processes will be terminated.\n"\
    "\n"


DIALOG_MODEL = {
    "default-response": 0,
    "icon-name": "dialog-warning-symbolic",
    "message": "",
    "buttons": (_("Cancel"), _("Close Application"))
    }


class DeltaTerminator(DeltaEntity):

    @classmethod
    def new_for_root_container(cls, root_container_as_parent):
        terminator = cls(root_container_as_parent)
        return terminator

    def _get_message_text(self, processes):
        message_text = MESSAGE_TEXT
        for process in processes:
            message_text += "<b>* {}</b>\n".format(process)
        return message_text

    def _get_user_confirmation(self, processes):
        message_text = self._get_message_text(processes)
        DIALOG_MODEL["message"] = message_text
        response = DeltaMessageDialog.run_for_model(self, DIALOG_MODEL)
        return response != 0

    def is_closable(self):
        processes = self._process_parser.parse()
        if processes:
            return self._get_user_confirmation(processes)
        else:
            return self._default_terminator.is_closable()

    def __init__(self, root_container):
        self._parent = root_container
        self._process_parser = FoxtrotProcessParser(root_container)
        self._default_terminator = self._enquiry("delta > default terminator")
